USE [master]
GO

/****** Object:  Database [Alan_Turing] ******/
CREATE DATABASE [Alan_Turing]
GO

USE [Alan_Turing]
GO

/****** Object:  Table [dbo].[Assignments] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assignments](
	[ID] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[datetime] [datetime] NOT NULL,
	[description] [text] NULL,
	[dueDate] [datetime] NOT NULL,
	[declarative_Lessons_ID] [int] NOT NULL,
	[courses_ID] [int] NOT NULL,
	[paths_ID] [int] NOT NULL,
	[lessons_ID] [int] NOT NULL,
	[assignments_identity] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Assignments_pk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[paths_ID] ASC,
	[courses_ID] ASC,
	[declarative_Lessons_ID] ASC,
	[lessons_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Chats] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chats](
	[message_id] [int] NOT NULL,
	[status] [bit] NOT NULL,
	[message_content] [varchar](1000) NOT NULL,
	[name] [varchar](7) NOT NULL,
	[date] [datetime] NOT NULL,
	[user_receiver_id] [int] NULL,
	[user_sender_id] [int] NULL,
	[group_receiver_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[message_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Comments] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[ID] [int] NOT NULL,
	[datetime] [datetime] NOT NULL,
	[content] [text] NOT NULL,
	[assignments_ID] [int] NULL,
	[users_ID] [int] NULL,
	[submissions_ID] [int] NULL,
	[paths_ID] [int] NULL,
	[courses_ID] [int] NULL,
	[declarative_Lessons_ID] [int] NULL,
	[lessons_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Courses] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Courses](
	[ID] [int] NOT NULL,
	[description] [varchar](300) NOT NULL,
	[price] [int] NOT NULL,
	[Subject] [varchar](50) NOT NULL,
	[paths_ID] [int] NOT NULL,
	[courses_identity] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Courses_pk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[paths_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Declarative_Lessons] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Declarative_Lessons](
	[ID] [int] NOT NULL,
	[description] [varchar](50) NULL,
	[name] [varchar](50) NOT NULL,
	[courses_ID] [int] NOT NULL,
	[paths_ID] [int] NOT NULL,
	[Declarative_Lessons_identity] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Declarative_Lessons_pk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[paths_ID] ASC,
	[courses_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Employees] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[id] [int] NOT NULL,
	[salary] [money] NOT NULL,
	[position] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Events] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Events](
	[id] [int] NOT NULL,
	[datetime] [datetime] NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](500) NULL,
	[category] [varchar](50) NULL,
 CONSTRAINT [PK__Events__3213E83F6B40B3F7] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Files] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Files](
	[folder_parentID] [int] NULL,
	[folder_childID] [int] NULL,
	[ID] [int] NOT NULL,
	[name] [varchar](100) NOT NULL,
	[desciption] [text] NULL,
	[type] [varchar](10) NOT NULL,
	[date] [date] NOT NULL,
	[accessibility] [bit] NULL,
	[content] [varbinary](max) NOT NULL,
 CONSTRAINT [PK__Files__3214EC27A55421D9] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[files_assignments] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[files_assignments](
	[files_ID] [int] NULL,
	[assignments_ID] [int] NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[files_learningMaterials] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[files_learningMaterials](
	[files_ID] [int] NULL,
	[learningMaterials_ID] [int] NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Finances] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Finances](
	[id] [int] NOT NULL,
	[datetime] [datetime] NOT NULL,
	[amount] [money] NOT NULL,
	[purpose] [varchar](20) NOT NULL,
	[receiver_id] [int] NOT NULL,
	[sender_id] [int] NOT NULL,
 CONSTRAINT [PK__Finances__3213E83F61473527] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Folders] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folders](
	[parentID] [int] NOT NULL,
	[name] [varchar](20) NOT NULL,
	[accessibility] [bit] NOT NULL,
	[childID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[parentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Graduations] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Graduations](
	[ID] [int] NOT NULL,
	[description] [varchar](300) NOT NULL,
	[EndDate] [date] NOT NULL,
	[StartDate] [date] NOT NULL,
	[paths_ID] [int] NOT NULL,
	[graduations_identity] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [pk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[paths_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Group_ConsistsOf_Students] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Group_ConsistsOf_Students](
	[students_ID] [int] NULL,
	[Groups_ID] [int] NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Groups] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Groups](
	[ID] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[stream_ID] [int] NULL,
	[Paths_ID] [int] NULL,
	[courses_ID] [int] NULL,
	[Lecturers_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[LearningMaterials] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LearningMaterials](
	[ID] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[date] [date] NOT NULL,
	[type] [varchar](10) NOT NULL,
	[paths_ID] [int] NULL,
	[courses_ID] [int] NULL,
	[declarative_Lessons_ID] [int] NULL,
	[lessons_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Lecturers] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lecturers](
	[id] [int] NOT NULL,
	[hourlyRate] [money] NOT NULL,
	[speciality] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Lessons] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lessons](
	[ID] [int] NOT NULL,
	[MeetingID] [varchar](256) NOT NULL,
	[dateTime] [datetime] NOT NULL,
	[duration] [time](0) NOT NULL,
	[description] [varchar](300) NULL,
	[declarative_Lessons_ID] [int] NOT NULL,
	[courses_ID] [int] NOT NULL,
	[paths_ID] [int] NOT NULL,
	[Lecturers_ID] [int] NULL,
	[Groups_ID] [int] NULL,
	[Streams_ID] [int] NULL,
 CONSTRAINT [Lessons_pk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[paths_ID] ASC,
	[courses_ID] ASC,
	[declarative_Lessons_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[notification_groups] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[notification_groups](
	[groups_ID] [int] NULL,
	[notifications_ID] [int] NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[notification_user] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[notification_user](
	[users_ID] [int] NULL,
	[notifications_ID] [int] NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Notifications] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notifications](
	[id] [int] NOT NULL,
	[content] [varchar](200) NOT NULL,
	[status] [bit] NOT NULL,
	[date] [datetime] NOT NULL,
	[name] [varchar](50) NULL,
	[category] [varchar](50) NULL,
	[submissions_id] [int] NULL,
	[assignments_id] [int] NULL,
	[events_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Paths] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Paths](
	[ID] [int] NOT NULL,
	[description] [varchar](50) NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Streams] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Streams](
	[ID] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[paths_ID] [int] NOT NULL,
	[courses_ID] [int] NOT NULL,
	[strearms_identity] [int] IDENTITY(1,1) NOT NULL,
	[end_date] [date] NOT NULL,
 CONSTRAINT [Streams_pk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[paths_ID] ASC,
	[courses_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Student_Participate_Lessons] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Participate_Lessons](
	[ID] [int] NOT NULL,
	[paths_ID] [int] NOT NULL,
	[courses_ID] [int] NOT NULL,
	[declarative_Lessons_ID] [int] NOT NULL,
	[lessons_ID] [int] NOT NULL,
	[students_ID] [int] NOT NULL,
	[mark] [int] NULL,
	[attendance] [bit] NULL,
	[Students_Participate_Lessons_identity] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Students_Participate_Lessons_pk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[paths_ID] ASC,
	[courses_ID] ASC,
	[declarative_Lessons_ID] ASC,
	[lessons_ID] ASC,
	[students_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Students] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Students](
	[id] [int] NOT NULL,
	[discount] [smallint] NULL,
	[rating] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Submissions] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Submissions](
	[ID] [int] NOT NULL,
	[datetime] [datetime] NOT NULL,
	[description] [text] NULL,
	[declarative_Lessons_ID] [int] NOT NULL,
	[courses_ID] [int] NOT NULL,
	[paths_ID] [int] NOT NULL,
	[lessons_ID] [int] NOT NULL,
	[assignments_ID] [int] NOT NULL,
	[students_ID] [int] NULL,
	[mark] [int] NOT NULL,
	[submissions_identity] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Submissions_pk] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[paths_ID] ASC,
	[courses_ID] ASC,
	[declarative_Lessons_ID] ASC,
	[lessons_ID] ASC,
	[assignments_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Users] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[gender] [varchar](10) NULL,
	[DOB] [date] NULL,
	[additional_info] [varchar](200) NULL,
	[status] [bit] NULL,
	[firstName] [varchar](20) NULL,
	[lastName] [varchar](20) NULL,
	[mail] [varchar](50) NOT NULL,
	[password] [varchar](70) NOT NULL,
	[picture] [image] NULL,
	[phone] [char](9) NULL,
	[cv] [varbinary](max) NULL,
 CONSTRAINT [PK__Users__3213E83F7D8171D1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Assignments] ON 

INSERT [dbo].[Assignments] ([ID], [name], [datetime], [description], [dueDate], [declarative_Lessons_ID], [courses_ID], [paths_ID], [lessons_ID], [assignments_identity]) VALUES (1, N'English for IT', CAST(N'2021-01-10T10:00:00.000' AS DateTime), NULL, CAST(N'2021-01-12T10:00:00.000' AS DateTime), 1, 1, 1, 1, 3)
SET IDENTITY_INSERT [dbo].[Assignments] OFF
GO
INSERT [dbo].[Chats] ([message_id], [status], [message_content], [name], [date], [user_receiver_id], [user_sender_id], [group_receiver_id]) VALUES (1, 0, N'abcd', N'text', CAST(N'2021-02-12T10:00:00.000' AS DateTime), NULL, 1, 1)
INSERT [dbo].[Chats] ([message_id], [status], [message_content], [name], [date], [user_receiver_id], [user_sender_id], [group_receiver_id]) VALUES (2, 0, N'hello', N'text', CAST(N'2021-01-13T00:00:00.000' AS DateTime), 1, 3, NULL)
GO
INSERT [dbo].[Comments] ([ID], [datetime], [content], [assignments_ID], [users_ID], [submissions_ID], [paths_ID], [courses_ID], [declarative_Lessons_ID], [lessons_ID]) VALUES (1, CAST(N'2021-02-11T12:00:00.000' AS DateTime), N'aaa', 1, 1, 1, 1, 1, 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Courses] ON 

INSERT [dbo].[Courses] ([ID], [description], [price], [Subject], [paths_ID], [courses_identity]) VALUES (1, N'English for programming', 7000, N'english', 1, 1)
SET IDENTITY_INSERT [dbo].[Courses] OFF
GO
SET IDENTITY_INSERT [dbo].[Declarative_Lessons] ON 

INSERT [dbo].[Declarative_Lessons] ([ID], [description], [name], [courses_ID], [paths_ID], [Declarative_Lessons_identity]) VALUES (1, N'Welcome to english for IT', N'English for IT', 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Declarative_Lessons] OFF
GO
INSERT [dbo].[Employees] ([id], [salary], [position]) VALUES (3, 100.0000, N'accountant')
GO
INSERT [dbo].[Events] ([id], [datetime], [name], [description], [category]) VALUES (1, CAST(N'2021-02-03T00:00:00.000' AS DateTime), N' Programming languages', NULL, N'languages')
GO
INSERT [dbo].[Files] ([folder_parentID], [folder_childID], [ID], [name], [desciption], [type], [date], [accessibility], [content]) VALUES (1, NULL, 1, N'Submissions', NULL, N'pdf', CAST(N'2021-01-11' AS Date), 1, 0x00ABBB28)
INSERT [dbo].[Files] ([folder_parentID], [folder_childID], [ID], [name], [desciption], [type], [date], [accessibility], [content]) VALUES (1, NULL, 2, N'Learning Materials for group1', NULL, N'pdf', CAST(N'2021-01-10' AS Date), 1, 0x0A0000012EF8214501000000)
GO
INSERT [dbo].[files_assignments] ([files_ID], [assignments_ID]) VALUES (1, 1)
GO
INSERT [dbo].[files_learningMaterials] ([files_ID], [learningMaterials_ID]) VALUES (2, 1)
GO
INSERT [dbo].[Finances] ([id], [datetime], [amount], [purpose], [receiver_id], [sender_id]) VALUES (1, CAST(N'2021-02-08T10:00:00.000' AS DateTime), 50.0000, N'Go path', 3, 1)
GO
INSERT [dbo].[Folders] ([parentID], [name], [accessibility], [childID]) VALUES (1, N'English for group1', 1, NULL)
GO
SET IDENTITY_INSERT [dbo].[Graduations] ON 

INSERT [dbo].[Graduations] ([ID], [description], [EndDate], [StartDate], [paths_ID], [graduations_identity]) VALUES (1, N'Go is a statically typed, compiled programming language designed at Google by Robert Griesemer, Rob Pike, and Ken Thompson. Go is syntactically similar to C, but with memory safety, garbage collection, structural typing, and CSP-style concurrency. ', CAST(N'2021-04-08' AS Date), CAST(N'2021-01-08' AS Date), 1, 1)
SET IDENTITY_INSERT [dbo].[Graduations] OFF
GO
INSERT [dbo].[Groups] ([ID], [name], [stream_ID], [Paths_ID], [courses_ID], [Lecturers_ID]) VALUES (1, N'English Group1', 1, 1, 1, 2)
GO
INSERT [dbo].[LearningMaterials] ([ID], [name], [date], [type], [paths_ID], [courses_ID], [declarative_Lessons_ID], [lessons_ID]) VALUES (1, N'English for IT', CAST(N'2021-01-10' AS Date), N'pdf', 1, 1, 1, 1)
GO
INSERT [dbo].[Lecturers] ([id], [hourlyRate], [speciality]) VALUES (2, 4000.0000, N'English Lecturer')
GO
INSERT [dbo].[Lessons] ([ID], [MeetingID], [dateTime], [duration], [description], [declarative_Lessons_ID], [courses_ID], [paths_ID], [Lecturers_ID], [Groups_ID], [Streams_ID]) VALUES (1, N'1456s6d5dsd6d6', CAST(N'2021-02-12T00:00:00.000' AS DateTime), CAST(N'01:00:00' AS Time), N'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry  standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ', 1, 1, 1, 2, 1, 1)
GO
INSERT [dbo].[notification_groups] ([groups_ID], [notifications_ID]) VALUES (1, 1)
GO
INSERT [dbo].[notification_user] ([users_ID], [notifications_ID]) VALUES (1, 1)
GO
INSERT [dbo].[Notifications] ([id], [content], [status], [date], [name], [category], [submissions_id], [assignments_id], [events_id]) VALUES (1, N'you have assignment', 0, CAST(N'2021-01-10T10:00:00.000' AS DateTime), N'assignment', N'assignment', NULL, 1, NULL)
GO
INSERT [dbo].[Paths] ([ID], [description], [name]) VALUES (1, N'has 7 courses', N'Go')
GO
SET IDENTITY_INSERT [dbo].[Streams] ON 

INSERT [dbo].[Streams] ([ID], [start_date], [paths_ID], [courses_ID], [strearms_identity], [end_date]) VALUES (1, CAST(N'2021-01-08' AS Date), 1, 1, 1, CAST(N'2021-02-08' AS Date))
SET IDENTITY_INSERT [dbo].[Streams] OFF
GO
INSERT [dbo].[Students] ([id], [discount], [rating]) VALUES (1, 20, 70)
GO
SET IDENTITY_INSERT [dbo].[Submissions] ON 

INSERT [dbo].[Submissions] ([ID], [datetime], [description], [declarative_Lessons_ID], [courses_ID], [paths_ID], [lessons_ID], [assignments_ID], [students_ID], [mark], [submissions_identity]) VALUES (1, CAST(N'2021-01-11T12:00:00.000' AS DateTime), NULL, 1, 1, 1, 1, 1, 1, 80, 1)
SET IDENTITY_INSERT [dbo].[Submissions] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([id], [gender], [DOB], [additional_info], [status], [firstName], [lastName], [mail], [password], [picture], [phone], [cv]) VALUES (1, N'female', CAST(N'1999-11-13' AS Date), N'I am a student', 1, N'Ruzanna', N'Baghdasaryan', N'ruzanna.bagdasaryan.99@gmail.com', N'1234', NULL, N'077454025', NULL)
INSERT [dbo].[Users] ([id], [gender], [DOB], [additional_info], [status], [firstName], [lastName], [mail], [password], [picture], [phone], [cv]) VALUES (2, N'male', CAST(N'1987-11-02' AS Date), N'I am a lecturer', 1, N'Armen', N'Karapetyan', N'armenkarapetyan.87@gmail.com', N'1122', NULL, N'94748454 ', NULL)
INSERT [dbo].[Users] ([id], [gender], [DOB], [additional_info], [status], [firstName], [lastName], [mail], [password], [picture], [phone], [cv]) VALUES (3, N'female', CAST(N'1985-04-02' AS Date), N'I am a employer', 1, N'Anna', N'Ohanyan', N'AnnaOhanyan85@gmail.com', N'1112', NULL, N'55478512 ', NULL)

SET IDENTITY_INSERT [dbo].[Users] OFF
GO
/****** Object:  Index [UQ__Assignme__3214EC26AF219DEB] ******/
ALTER TABLE [dbo].[Assignments] ADD UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Courses__3214EC26ABD85FA4] ******/
ALTER TABLE [dbo].[Courses] ADD  CONSTRAINT [UQ__Courses__3214EC26ABD85FA4] UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Declarat__3214EC262F289AFE] ******/
ALTER TABLE [dbo].[Declarative_Lessons] ADD UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Graduati__3214EC26F16DDBC7] ******/
ALTER TABLE [dbo].[Graduations] ADD  CONSTRAINT [UQ__Graduati__3214EC26F16DDBC7] UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Lessons__3214EC26438CC650] ******/
ALTER TABLE [dbo].[Lessons] ADD  CONSTRAINT [UQ__Lessons__3214EC26438CC650] UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Paths__72E12F1BF50AD3B6] ******/
ALTER TABLE [dbo].[Paths] ADD UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Streams__3214EC26827AA00E] ******/
ALTER TABLE [dbo].[Streams] ADD  CONSTRAINT [UQ__Streams__3214EC26827AA00E] UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UQ__Submissi__3214EC26137F5936] ******/
ALTER TABLE [dbo].[Submissions] ADD UNIQUE NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Users__7A2129048A1A65BF] ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [UQ__Users__7A2129048A1A65BF] UNIQUE NONCLUSTERED 
(
	[mail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__cours__7E37BEF6] FOREIGN KEY([courses_ID])
REFERENCES [dbo].[Courses] ([ID])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK__Assignmen__cours__7E37BEF6]
GO
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD FOREIGN KEY([declarative_Lessons_ID])
REFERENCES [dbo].[Declarative_Lessons] ([ID])
GO
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD  CONSTRAINT [FK__Assignmen__lesso__00200768] FOREIGN KEY([lessons_ID])
REFERENCES [dbo].[Lessons] ([ID])
GO
ALTER TABLE [dbo].[Assignments] CHECK CONSTRAINT [FK__Assignmen__lesso__00200768]
GO
ALTER TABLE [dbo].[Assignments]  WITH CHECK ADD FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Chats]  WITH CHECK ADD FOREIGN KEY([group_receiver_id])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[Chats]  WITH CHECK ADD  CONSTRAINT [FK__Chats__user_rece__32AB8735] FOREIGN KEY([user_receiver_id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Chats] CHECK CONSTRAINT [FK__Chats__user_rece__32AB8735]
GO
ALTER TABLE [dbo].[Chats]  WITH CHECK ADD  CONSTRAINT [FK__Chats__user_send__339FAB6E] FOREIGN KEY([user_sender_id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Chats] CHECK CONSTRAINT [FK__Chats__user_send__339FAB6E]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD FOREIGN KEY([assignments_ID])
REFERENCES [dbo].[Assignments] ([ID])
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK__Comments__course__0F624AF8] FOREIGN KEY([courses_ID])
REFERENCES [dbo].[Courses] ([ID])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK__Comments__course__0F624AF8]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD FOREIGN KEY([declarative_Lessons_ID])
REFERENCES [dbo].[Declarative_Lessons] ([ID])
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK__Comments__lesson__114A936A] FOREIGN KEY([lessons_ID])
REFERENCES [dbo].[Lessons] ([ID])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK__Comments__lesson__114A936A]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD FOREIGN KEY([submissions_ID])
REFERENCES [dbo].[Submissions] ([ID])
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK__Comments__users___0C85DE4D] FOREIGN KEY([users_ID])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK__Comments__users___0C85DE4D]
GO
ALTER TABLE [dbo].[Courses]  WITH CHECK ADD  CONSTRAINT [FK__Courses__paths_I__52593CB8] FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Courses] CHECK CONSTRAINT [FK__Courses__paths_I__52593CB8]
GO
ALTER TABLE [dbo].[Declarative_Lessons]  WITH CHECK ADD  CONSTRAINT [FK__Declarati__cours__5AEE82B9] FOREIGN KEY([courses_ID])
REFERENCES [dbo].[Courses] ([ID])
GO
ALTER TABLE [dbo].[Declarative_Lessons] CHECK CONSTRAINT [FK__Declarati__cours__5AEE82B9]
GO
ALTER TABLE [dbo].[Declarative_Lessons]  WITH CHECK ADD FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK__Employees__id__778AC167] FOREIGN KEY([id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK__Employees__id__778AC167]
GO
ALTER TABLE [dbo].[Files]  WITH CHECK ADD  CONSTRAINT [FK__Files__folder_pa__160F4887] FOREIGN KEY([folder_parentID])
REFERENCES [dbo].[Folders] ([parentID])
GO
ALTER TABLE [dbo].[Files] CHECK CONSTRAINT [FK__Files__folder_pa__160F4887]
GO
ALTER TABLE [dbo].[files_assignments]  WITH CHECK ADD FOREIGN KEY([assignments_ID])
REFERENCES [dbo].[Assignments] ([ID])
GO
ALTER TABLE [dbo].[files_assignments]  WITH CHECK ADD  CONSTRAINT [FK__files_ass__files__17F790F9] FOREIGN KEY([files_ID])
REFERENCES [dbo].[Files] ([ID])
GO
ALTER TABLE [dbo].[files_assignments] CHECK CONSTRAINT [FK__files_ass__files__17F790F9]
GO
ALTER TABLE [dbo].[files_learningMaterials]  WITH CHECK ADD  CONSTRAINT [FK__files_lea__files__208CD6FA] FOREIGN KEY([files_ID])
REFERENCES [dbo].[Files] ([ID])
GO
ALTER TABLE [dbo].[files_learningMaterials] CHECK CONSTRAINT [FK__files_lea__files__208CD6FA]
GO
ALTER TABLE [dbo].[files_learningMaterials]  WITH CHECK ADD FOREIGN KEY([learningMaterials_ID])
REFERENCES [dbo].[LearningMaterials] ([ID])
GO
ALTER TABLE [dbo].[Finances]  WITH CHECK ADD  CONSTRAINT [FK__Finances__receiv__2EDAF651] FOREIGN KEY([receiver_id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Finances] CHECK CONSTRAINT [FK__Finances__receiv__2EDAF651]
GO
ALTER TABLE [dbo].[Finances]  WITH CHECK ADD  CONSTRAINT [FK__Finances__sender__2FCF1A8A] FOREIGN KEY([sender_id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Finances] CHECK CONSTRAINT [FK__Finances__sender__2FCF1A8A]
GO
ALTER TABLE [dbo].[Graduations]  WITH CHECK ADD  CONSTRAINT [FK__Graduatio__paths__4D94879B] FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Graduations] CHECK CONSTRAINT [FK__Graduatio__paths__4D94879B]
GO
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD  CONSTRAINT [FK__Groups__courses___6754599E] FOREIGN KEY([courses_ID])
REFERENCES [dbo].[Courses] ([ID])
GO
ALTER TABLE [dbo].[Groups] CHECK CONSTRAINT [FK__Groups__courses___6754599E]
GO
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD FOREIGN KEY([Lecturers_ID])
REFERENCES [dbo].[Lecturers] ([id])
GO
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD FOREIGN KEY([Paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD  CONSTRAINT [FK__Groups__stream_I__656C112C] FOREIGN KEY([stream_ID])
REFERENCES [dbo].[Streams] ([ID])
GO
ALTER TABLE [dbo].[Groups] CHECK CONSTRAINT [FK__Groups__stream_I__656C112C]
GO
ALTER TABLE [dbo].[LearningMaterials]  WITH CHECK ADD  CONSTRAINT [FK__LearningM__cours__1CBC4616] FOREIGN KEY([courses_ID])
REFERENCES [dbo].[Courses] ([ID])
GO
ALTER TABLE [dbo].[LearningMaterials] CHECK CONSTRAINT [FK__LearningM__cours__1CBC4616]
GO
ALTER TABLE [dbo].[LearningMaterials]  WITH CHECK ADD FOREIGN KEY([declarative_Lessons_ID])
REFERENCES [dbo].[Declarative_Lessons] ([ID])
GO
ALTER TABLE [dbo].[LearningMaterials]  WITH CHECK ADD  CONSTRAINT [FK__LearningM__lesso__1EA48E88] FOREIGN KEY([lessons_ID])
REFERENCES [dbo].[Lessons] ([ID])
GO
ALTER TABLE [dbo].[LearningMaterials] CHECK CONSTRAINT [FK__LearningM__lesso__1EA48E88]
GO
ALTER TABLE [dbo].[LearningMaterials]  WITH CHECK ADD FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Lecturers]  WITH CHECK ADD  CONSTRAINT [FK__Lecturers__id__628FA481] FOREIGN KEY([id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Lecturers] CHECK CONSTRAINT [FK__Lecturers__id__628FA481]
GO
ALTER TABLE [dbo].[Lessons]  WITH CHECK ADD  CONSTRAINT [FK__Lessons__courses__6E01572D] FOREIGN KEY([courses_ID])
REFERENCES [dbo].[Courses] ([ID])
GO
ALTER TABLE [dbo].[Lessons] CHECK CONSTRAINT [FK__Lessons__courses__6E01572D]
GO
ALTER TABLE [dbo].[Lessons]  WITH CHECK ADD  CONSTRAINT [FK__Lessons__declara__6D0D32F4] FOREIGN KEY([declarative_Lessons_ID])
REFERENCES [dbo].[Declarative_Lessons] ([ID])
GO
ALTER TABLE [dbo].[Lessons] CHECK CONSTRAINT [FK__Lessons__declara__6D0D32F4]
GO
ALTER TABLE [dbo].[Lessons]  WITH CHECK ADD  CONSTRAINT [FK__Lessons__Groups___70DDC3D8] FOREIGN KEY([Groups_ID])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[Lessons] CHECK CONSTRAINT [FK__Lessons__Groups___70DDC3D8]
GO
ALTER TABLE [dbo].[Lessons]  WITH CHECK ADD  CONSTRAINT [FK__Lessons__Lecture__6FE99F9F] FOREIGN KEY([Lecturers_ID])
REFERENCES [dbo].[Lecturers] ([id])
GO
ALTER TABLE [dbo].[Lessons] CHECK CONSTRAINT [FK__Lessons__Lecture__6FE99F9F]
GO
ALTER TABLE [dbo].[Lessons]  WITH CHECK ADD  CONSTRAINT [FK__Lessons__paths_I__6EF57B66] FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Lessons] CHECK CONSTRAINT [FK__Lessons__paths_I__6EF57B66]
GO
ALTER TABLE [dbo].[Lessons]  WITH CHECK ADD  CONSTRAINT [FK__Lessons__Streams__71D1E811] FOREIGN KEY([Streams_ID])
REFERENCES [dbo].[Streams] ([ID])
GO
ALTER TABLE [dbo].[Lessons] CHECK CONSTRAINT [FK__Lessons__Streams__71D1E811]
GO
ALTER TABLE [dbo].[notification_groups]  WITH CHECK ADD FOREIGN KEY([groups_ID])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[notification_groups]  WITH CHECK ADD FOREIGN KEY([notifications_ID])
REFERENCES [dbo].[Notifications] ([id])
GO
ALTER TABLE [dbo].[notification_user]  WITH CHECK ADD FOREIGN KEY([notifications_ID])
REFERENCES [dbo].[Notifications] ([id])
GO
ALTER TABLE [dbo].[notification_user]  WITH CHECK ADD  CONSTRAINT [FK__notificat__users__367C1819] FOREIGN KEY([users_ID])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[notification_user] CHECK CONSTRAINT [FK__notificat__users__367C1819]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD FOREIGN KEY([assignments_id])
REFERENCES [dbo].[Assignments] ([ID])
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD  CONSTRAINT [FK__Notificat__event__2BFE89A6] FOREIGN KEY([events_id])
REFERENCES [dbo].[Events] ([id])
GO
ALTER TABLE [dbo].[Notifications] CHECK CONSTRAINT [FK__Notificat__event__2BFE89A6]
GO
ALTER TABLE [dbo].[Notifications]  WITH CHECK ADD FOREIGN KEY([submissions_id])
REFERENCES [dbo].[Submissions] ([ID])
GO
ALTER TABLE [dbo].[Streams]  WITH CHECK ADD  CONSTRAINT [FK__Streams__courses__571DF1D5] FOREIGN KEY([courses_ID])
REFERENCES [dbo].[Courses] ([ID])
GO
ALTER TABLE [dbo].[Streams] CHECK CONSTRAINT [FK__Streams__courses__571DF1D5]
GO
ALTER TABLE [dbo].[Streams]  WITH CHECK ADD  CONSTRAINT [FK__Streams__paths_I__5629CD9C] FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Streams] CHECK CONSTRAINT [FK__Streams__paths_I__5629CD9C]
GO
ALTER TABLE [dbo].[Students]  WITH CHECK ADD  CONSTRAINT [FK__Students__id__74AE54BC] FOREIGN KEY([id])
REFERENCES [dbo].[Users] ([id])
GO
ALTER TABLE [dbo].[Students] CHECK CONSTRAINT [FK__Students__id__74AE54BC]
GO
ALTER TABLE [dbo].[Submissions]  WITH CHECK ADD FOREIGN KEY([assignments_ID])
REFERENCES [dbo].[Assignments] ([ID])
GO
ALTER TABLE [dbo].[Submissions]  WITH CHECK ADD  CONSTRAINT [FK__Submissio__cours__04E4BC85] FOREIGN KEY([courses_ID])
REFERENCES [dbo].[Courses] ([ID])
GO
ALTER TABLE [dbo].[Submissions] CHECK CONSTRAINT [FK__Submissio__cours__04E4BC85]
GO
ALTER TABLE [dbo].[Submissions]  WITH CHECK ADD FOREIGN KEY([declarative_Lessons_ID])
REFERENCES [dbo].[Declarative_Lessons] ([ID])
GO
ALTER TABLE [dbo].[Submissions]  WITH CHECK ADD  CONSTRAINT [FK__Submissio__lesso__06CD04F7] FOREIGN KEY([lessons_ID])
REFERENCES [dbo].[Lessons] ([ID])
GO
ALTER TABLE [dbo].[Submissions] CHECK CONSTRAINT [FK__Submissio__lesso__06CD04F7]
GO
ALTER TABLE [dbo].[Submissions]  WITH CHECK ADD FOREIGN KEY([paths_ID])
REFERENCES [dbo].[Paths] ([ID])
GO
ALTER TABLE [dbo].[Submissions]  WITH CHECK ADD FOREIGN KEY([students_ID])
REFERENCES [dbo].[Students] ([id])
GO
USE [master]
GO
ALTER DATABASE [Alan_Turing] SET  READ_WRITE 
GO
