package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"gitlab.com/alanturingschool/lms/lms-core/Go/Chat/authentication"
	"gitlab.com/alanturingschool/lms/lms-core/Go/Chat/config"
	"github.com/gorilla/mux"
)

// User - Our struct for all Users"
type User struct {
	ID        int    `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

// Chat - Our struct for all chats"
type Chat struct {
	MessageID int `json:"message_id"`
	//	Status          string    `json:"status"`
	MessageContent  string    `json:"message_content"`
	Date            time.Time `json:"date"`
	UserReceiverID  NullInt32 `json:"user_receiver_id"`
	UserSenderID    NullInt32 `json:"user_sender_id"`
	GroupReceiverID NullInt32 `json:"group_receiver_id"`
}

// Group - Our struct for all Groups"
type Group struct {
	ID   int    `json:"id"`
	Name string `json:"Name"`
}

// GroupConsistsOfStudent - Our struct for all groups"
type GroupConsistsOfStudent struct {
	StudentID int `json:"student_id"`
	GroupID   int `json:"group_id"`
}

// CUSTOM NULL Handling structures

// NullInt32 is an alias for sql.NullInt64 data type
type NullInt32 sql.NullInt32

// Scan implements the Scanner interface for NullInt64
func (ni *NullInt32) Scan(value interface{}) error {
	var i sql.NullInt32
	if err := i.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ni = NullInt32{i.Int32, false}
	} else {
		*ni = NullInt32{i.Int32, true}
	}
	return nil
}

// MarshalJSON for NullInt32
func (ni *NullInt32) MarshalJSON() ([]byte, error) {
	if !ni.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(ni.Int32)
}

// UnmarshalJSON for NullInt32
func (ni *NullInt32) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &ni.Int32)
	ni.Valid = (err == nil)
	return err
}

//NullString is  an alias for sql.Nullstring data type
type NullString sql.NullString

// Scan implements the Scanner interface for NullString
func (ns *NullString) Scan(value interface{}) error {
	var s sql.NullString
	if err := s.Scan(value); err != nil {
		return err
	}

	// if nil then make Valid false
	if reflect.TypeOf(value) == nil {
		*ns = NullString{s.String, false}
	} else {
		*ns = NullString{s.String, true}
	}

	return nil
}

// MarshalJSON for NullString
func (ns *NullString) MarshalJSON() ([]byte, error) {
	if !ns.Valid {
		return []byte("null"), nil
	}
	return json.Marshal(ns.String)
}

// UnmarshalJSON for NullString
func (ns *NullString) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &ns.String)
	ns.Valid = (err == nil)
	return err
}

func chatPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the chatting!")
}

func returnAllUser(w http.ResponseWriter, r *http.Request) {
	var allUser []User
	fmt.Println("Endpoint Hit: returnAllUser")
	json.NewEncoder(w).Encode(allUser)
}

func searchChatUser(w http.ResponseWriter, r *http.Request) {

	setupCorsResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}

	vars := mux.Vars(r)
	key := vars["key"]
	fmt.Println(key)
	fmt.Println("aaaaaa")
	Config, _ := config.LoadConfiguration("conf.json")
	//	db, err := sql.Open("mssql", "Database=Alan_Turing; Trusted_Connection=True;")
	db, err := sql.Open("mssql", "Server="+Config.Database.Server+";Database="+Config.Database.Database+";User Id="+Config.Database.UserName+";PASSWORD="+Config.Database.Password+"")
	if err != nil {
		fmt.Println("database could not opened!!!!")
		fmt.Println(err.Error())
		return
	}
	userID := strconv.Itoa(authentication.GetUser(1321))

	var count int
	var queryCount = "(select count(*) as count from (SELECT distinct  id,CONCAT(Users.firstName , ' ' , Users.lastName) as Name FROM Users JOIN  Chats on Users.id=Chats.user_sender_id or Users.id=Chats.user_receiver_id WHERE  (Chats.user_sender_id=" + userID + " or Chats.user_receiver_id =" + userID + " )	   and (Users.firstName like '%" + key + "%' or Users.lastName like '%" + key + "%') UNION ALL select distinct top 10 Group_ConsistsOf_Students.Groups_ID,Groups.name  from Groups join Group_ConsistsOf_Students on Groups.ID = Group_ConsistsOf_Students.Groups_ID where Group_ConsistsOf_Students.students_ID =" + userID + " and Groups.name like '%" + key + "%')as count)"

	stmt, err := db.Query(queryCount)
	//fmt.Println(queryCount)

	if err != nil {
		fmt.Println("Query1 failed.....")
		fmt.Println(err.Error())
		return
	}
	if stmt.Next() {
		if err = stmt.Scan(&count); err != nil {
			fmt.Println("Scanning1 failed.....")
			fmt.Println(err.Error())
			return
		}
		fmt.Println(count)
		stmt.Close()

	}

	var querySelect = "SELECT distinct  id,CONCAT(Users.firstName , ' ' , Users.lastName) as Name FROM Users JOIN  Chats on Users.id=Chats.user_sender_id or Users.id=Chats.user_receiver_id WHERE  (Chats.user_sender_id=" + userID + " or Chats.user_receiver_id =" + userID + " ) and (Users.firstName like '%" + key + "%' or Users.lastName like '%" + key + "%') UNION ALL select distinct top 10 Group_ConsistsOf_Students.Groups_ID,Groups.name  from Groups join Group_ConsistsOf_Students on Groups.ID = Group_ConsistsOf_Students.Groups_ID where Group_ConsistsOf_Students.students_ID =" + userID + " and Groups.name like '%" + key + "%'"
	//fmt.Println(querySelect)

	rows, err := db.Query(querySelect)
	if err != nil {
		fmt.Println("Query failed.....")
		fmt.Println(err.Error())
		return
	}

	Users := make([]User, count)
	i := 0
	for rows.Next() {

		var user User
		if err = rows.Scan(&user.ID, &user.FirstName); err != nil {
			fmt.Println("Scanning failed.....")
			fmt.Println(err.Error())
			return
		}

		Users[i] = user
		i++
	}

	fmt.Printf("Users Instance := %#v\n", Users)
	json.NewEncoder(w).Encode(Users)

}

func returnUserChat(w http.ResponseWriter, r *http.Request) {
	setupCorsResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	vars := mux.Vars(r)
	correspondentID := vars["correspondentID"]

	fmt.Println(correspondentID)
	Config, _ := config.LoadConfiguration("conf.json")
	//	db, err := sql.Open("mssql", "Database=Alan_Turing; Trusted_Connection=True;")
	db, err := sql.Open("mssql", "Server="+Config.Database.Server+";Database="+Config.Database.Database+";User Id="+Config.Database.UserName+";PASSWORD="+Config.Database.Password+"")
	if err != nil {
		fmt.Println("database could not opened!!!!")
		fmt.Println(err.Error())
		return
	}

	userID := strconv.Itoa(authentication.GetUser(1321))

	var count int
	var chatsCount = "(select count(*) from (select distinct  Message_ID, Message_Content, Chats.date , user_receiver_id ,chats.user_sender_id from Chats join Users on Chats.user_receiver_id= Users.id or  Chats.user_sender_id= Users.id where  (Chats.user_sender_id = " + userID + " and  Chats.user_receiver_id =  " + correspondentID + ") or( Chats.user_receiver_id = " + userID + " and Chats.user_sender_id = " + correspondentID + ")and users.id<>" + userID + ") as count)"

	stmt, err := db.Query(chatsCount)

	if err != nil {
		fmt.Println("Query1 failed.....")
		fmt.Println(err.Error())
		return
	}
	if stmt.Next() {
		if err = stmt.Scan(&count); err != nil {
			fmt.Println("Scanning1 failed.....")
			fmt.Println(err.Error())
			return
		}
		fmt.Println("Count =", count)
		stmt.Close()

	}

	var querySelect = "(select distinct  Message_ID, Message_Content, Chats.date , user_receiver_id ,chats.user_sender_id from Chats join Users on Chats.user_receiver_id= Users.id or  Chats.user_sender_id= Users.id where  (Chats.user_sender_id = " + userID + " and  Chats.user_receiver_id = " + correspondentID + ") or( Chats.user_receiver_id = " + userID + " and Chats.user_sender_id = " + correspondentID + ")and users.id<>" + userID + ")"
	//fmt.Println(querySelect)

	rows, err := db.Query(querySelect)
	if err != nil {
		fmt.Println("Query failed.....")
		fmt.Println(err.Error())
		return
	}
	Chats := make([]Chat, count)
	i := 0

	for rows.Next() {
		var chat Chat

		if err = rows.Scan(&chat.MessageID, &chat.MessageContent, &chat.Date, &chat.UserReceiverID, &chat.UserSenderID); err != nil {
			fmt.Println("Scanning failed.....")
			fmt.Println(err.Error())
			return
		}

		Chats[i] = chat
		i++

	}

	fmt.Printf("Users Instance := %#v\n", Chats)
	json.NewEncoder(w).Encode(Chats)

}

func returnGroupChat(w http.ResponseWriter, r *http.Request) {
	setupCorsResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	vars := mux.Vars(r)
	correspondentID := vars["correspondentID"]

	fmt.Println(correspondentID)
	Config, _ := config.LoadConfiguration("conf.json")
	//	db, err := sql.Open("mssql", "Database=Alan_Turing; Trusted_Connection=True;")
	db, err := sql.Open("mssql", "Server="+Config.Database.Server+";Database="+Config.Database.Database+";User Id="+Config.Database.UserName+";PASSWORD="+Config.Database.Password+"")
	if err != nil {
		fmt.Println("database could not opened!!!!")
		fmt.Println(err.Error())
		return
	}

	userID := strconv.Itoa(authentication.GetUser(1321))

	var count int
	var chatsCount = "(select count(*)from (select distinct Message_ID, Message_Content, Date ,chats.user_sender_id ,group_receiver_ID from Chats join Users on Chats.user_receiver_id= Users.id or  Chats.user_sender_id= Users.id join Groups on Groups.ID=Chats.group_receiver_id join Group_ConsistsOf_Students on Group_ConsistsOf_Students.Groups_ID=Chats.group_receiver_id where  Chats.group_receiver_id = " + correspondentID + " and Group_ConsistsOf_Students.students_ID= " + userID + "group by Message_ID, Message_Content, Date ,chats.user_sender_id ,group_receiver_ID ) as count)"

	stmt, err := db.Query(chatsCount)

	if err != nil {
		fmt.Println("Query1 failed.....")
		fmt.Println(err.Error())
		return
	}
	if stmt.Next() {
		if err = stmt.Scan(&count); err != nil {
			fmt.Println("Scanning1 failed.....")
			fmt.Println(err.Error())
			return
		}
		fmt.Println("Count =", count)
		stmt.Close()

	}

	var querySelect = "(select distinct Message_ID, Message_Content, Date ,chats.user_sender_id ,group_receiver_ID from Chats join Users on Chats.user_receiver_id= Users.id or  Chats.user_sender_id= Users.id join Groups on Groups.ID=Chats.group_receiver_id join Group_ConsistsOf_Students on Group_ConsistsOf_Students.Groups_ID=Chats.group_receiver_id where  Chats.group_receiver_id = " + correspondentID + " and Group_ConsistsOf_Students.students_ID=" + userID + "group by Message_ID, Message_Content, Date ,chats.user_sender_id ,group_receiver_ID )"
	//fmt.Println(querySelect)

	rows, err := db.Query(querySelect)
	if err != nil {
		fmt.Println("Query failed.....")
		fmt.Println(err.Error())
		return
	}
	Chats := make([]Chat, count)
	i := 0

	for rows.Next() {
		var chat Chat

		if err = rows.Scan(&chat.MessageID, &chat.MessageContent, &chat.Date, &chat.UserSenderID, &chat.GroupReceiverID); err != nil {
			fmt.Println("Scanning failed.....")
			fmt.Println(err.Error())
			return
		}

		Chats[i] = chat
		i++

	}

	fmt.Printf("Users Instance := %#v\n", Chats)
	json.NewEncoder(w).Encode(Chats)

}

func returnUserGroups(w http.ResponseWriter, r *http.Request) {
	setupCorsResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	//vars := mux.Vars(r)
	Config, _ := config.LoadConfiguration("conf.json")
	//db, err := sql.Open("mssql", "Database=Alan_Turing; Trusted_Connection=True;")
	db, err := sql.Open("mssql", "Server="+Config.Database.Server+";Database="+Config.Database.Database+";User Id="+Config.Database.UserName+";PASSWORD="+Config.Database.Password+"")
	if err != nil {
		fmt.Println("database could not opened!!!!")
		fmt.Println(err.Error())
		return
	}

	userID := strconv.Itoa(authentication.GetUser(1321))

	var count int
	var chatsCount = "(select Count(Groups.ID) from Groups Join Group_ConsistsOf_Students on Groups.ID= Group_ConsistsOf_Students.Groups_ID where Group_ConsistsOf_Students.students_ID =" + userID + ")"

	stmt, err := db.Query(chatsCount)

	if err != nil {
		fmt.Println("Query1 failed.....")
		fmt.Println(err.Error())
		return
	}
	if stmt.Next() {
		if err = stmt.Scan(&count); err != nil {
			fmt.Println("Scanning1 failed.....")
			fmt.Println(err.Error())
			return
		}
		fmt.Println("Count =", count)
		stmt.Close()

	}

	var querySelect = "(select Groups.ID,Groups.name from Groups Join Group_ConsistsOf_Students on Groups.ID= Group_ConsistsOf_Students.Groups_ID where Group_ConsistsOf_Students.students_ID =" + userID + ")"
	//fmt.Println(querySelect)

	rows, err := db.Query(querySelect)
	if err != nil {
		fmt.Println("Query failed.....")
		fmt.Println(err.Error())
		return
	}

	Groups := make([]Group, count)
	i := 0
	for rows.Next() {

		var group Group
		if err = rows.Scan(&group.ID, &group.Name); err != nil {
			fmt.Println("Scanning failed.....")
			fmt.Println(err.Error())
			return
		}

		Groups[i] = group
		i++
	}

	fmt.Printf("Users Instance := %#v\n", Groups)
	json.NewEncoder(w).Encode(Groups)

}

func returnChatList(w http.ResponseWriter, r *http.Request) {
	setupCorsResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	//vars := mux.Vars(r)
	Config, _ := config.LoadConfiguration("conf.json")
	//db, err := sql.Open("mssql", "Database=Alan_Turing; Trusted_Connection=True;")
	db, err := sql.Open("mssql", "Server="+Config.Database.Server+";Database="+Config.Database.Database+";User Id="+Config.Database.UserName+";PASSWORD="+Config.Database.Password+"")
	if err != nil {
		fmt.Println("database could not opened!!!!")
		fmt.Println(err.Error())
		return
	}

	userID := strconv.Itoa(authentication.GetUser(1321))

	var count int
	var chatsCount = " ((select count(*) as count from ((select distinct top 100 Users.id from Users join Chats on Users.id=Chats.user_receiver_id or Users.id=Chats.user_sender_id join ( select top 100 max(Chats.date) maxtime, Chats.user_sender_id ,Chats.user_receiver_id,Chats.group_receiver_id from Chats where Chats.user_sender_id = " + userID + " or Chats.user_receiver_id = " + userID + " group by Chats.user_sender_id,Chats.user_receiver_id,Chats.group_receiver_id order by maxtime desc) latest on Chats.date=latest.maxtime  and Chats.user_sender_id=latest.user_sender_id where Users.id<>" + userID + ") union all (select distinct top 100 Chats.group_receiver_id from Groups join Group_ConsistsOf_Students on Groups.ID = Group_ConsistsOf_Students.Groups_ID join Chats on Groups.ID = Chats.group_receiver_id join (select top 100 max(Chats.date) maxtime ,Chats.group_receiver_id from Chats group by Chats.group_receiver_id order by maxtime desc) latest on Chats.date=latest.maxtime and Chats.group_receiver_id= latest.group_receiver_id where Group_ConsistsOf_Students.students_ID =" + userID + ")) as count ))"

	stmt, err := db.Query(chatsCount)

	if err != nil {
		fmt.Println("Query1 failed.....")
		fmt.Println(err.Error())
		return
	}
	if stmt.Next() {
		if err = stmt.Scan(&count); err != nil {
			fmt.Println("Scanning1 failed.....")
			fmt.Println(err.Error())
			return
		}
		fmt.Println("Count =", count)
		stmt.Close()

	}

	var querySelect = "(((select  * from(select top 100  Users.id,max(Chats.date) as date,CONCAT(Users.firstName , ' ' , Users.lastName) as Name, ROW_NUMBER() OVER (PARTITION BY Users.firstname ORDER BY Chats.date desc) AS RowNumber from Users join Chats on Users.id=Chats.user_receiver_id or Users.id=Chats.user_sender_id join (select top 100 max(Chats.date) maxtime, Chats.user_sender_id ,Chats.user_receiver_id,Chats.group_receiver_id	from  Chats where Chats.user_sender_id =  " + userID + " or Chats.user_receiver_id =  " + userID + " group by Chats.user_sender_id,Chats.user_receiver_id,Chats.group_receiver_id order by maxtime desc) latest on Chats.date=latest.maxtime  and Chats.user_sender_id=latest.user_sender_id where  users.id<> " + userID + " group by Users.ID ,Users.firstName,Users.lastName,Chats.date order by Chats.date desc ) as a where a.RowNumber=1) union ALL (select * from (select top 100 Chats.group_receiver_id,max(Chats.date) as date,Groups.name, ROW_NUMBER() OVER (PARTITION BY Groups.name ORDER BY Chats.date desc) AS RowNumber  from Groups join Group_ConsistsOf_Students on Groups.ID = Group_ConsistsOf_Students.Groups_ID  join Chats on Groups.ID = Chats.group_receiver_id join (select top 100 max(Chats.date) maxtime ,Chats.group_receiver_id from Chats group by Chats.group_receiver_id order by maxtime desc) latest on Chats.date=latest.maxtime and Chats.group_receiver_id= latest.group_receiver_id  where Group_ConsistsOf_Students.students_ID = " + userID + " group by Chats.group_receiver_id,Groups.name,Chats.date  order by Chats.date desc )as b where b.RowNumber=1) )  )"

	//fmt.Println(querySelect)
	rows, err := db.Query(querySelect)
	if err != nil {
		fmt.Println("Query failed.....")
		fmt.Println(err.Error())
		return
	}
	Users := make([]User, count)
	Chats := make([]Chat, count)
	i := 0
	j := 0
	for rows.Next() {
		var chat Chat
		var user User
		var k int
		d := chat.Date
		if err = rows.Scan(&user.ID, &d, &user.FirstName, &k); err != nil {
			fmt.Println("Scanning failed.....")
			fmt.Println(err.Error())
			return
		}
		Users[i] = user
		i++
		Chats[j] = chat
		j++
	}
	fmt.Printf("Users Instance := %#v\n", Users)
	//	fmt.Printf("Users Instance := %#v\n", Chats)
	json.NewEncoder(w).Encode(Users)
	//	json.NewEncoder(w).Encode(Chats)

}

func returnUsersGroupsList(w http.ResponseWriter, r *http.Request) {
	setupCorsResponse(&w, r)
	if (*r).Method == "OPTIONS" {
		return
	}
	//vars := mux.Vars(r)
	Config, _ := config.LoadConfiguration("conf.json")
	//db, err := sql.Open("mssql", "Database=Alan_Turing; Trusted_Connection=True;")
	db, err := sql.Open("mssql", "Server="+Config.Database.Server+";Database="+Config.Database.Database+";User Id="+Config.Database.UserName+";PASSWORD="+Config.Database.Password+"")
	if err != nil {
		fmt.Println("database could not opened!!!!")
		fmt.Println(err.Error())
		return
	}

	userID := strconv.Itoa(authentication.GetUser(1321))

	var count int
	var chatsCount = " (select count(*)from ((select  * from(select top 100  Users.id,CONCAT(Users.firstName , ' ' , Users.lastName) as Name, ROW_NUMBER() OVER (PARTITION BY Users.firstname ORDER BY Chats.date desc) AS RowNumber from Users join Chats on Users.id=Chats.user_receiver_id or Users.id=Chats.user_sender_id join(select top 100 max(Chats.date) maxtime, Chats.user_sender_id ,Chats.user_receiver_id,Chats.group_receiver_id from  Chats where Chats.user_sender_id =" + userID + "  or Chats.user_receiver_id =" + userID + " group by Chats.user_sender_id,Chats.user_receiver_id,Chats.group_receiver_id order by maxtime desc) latest on Chats.date=latest.maxtime  and Chats.user_sender_id=latest.user_sender_id where  users.id<>" + userID + " group by Users.ID ,Users.firstName,Users.lastName,Chats.date ) as a where a.RowNumber=1) union ALL(select *from (select top 100 Group_ConsistsOf_Students.Groups_ID,Groups.name, ROW_NUMBER() OVER (PARTITION BY Groups.name ORDER BY groups.id desc) AS RowNumber from Groups join Group_ConsistsOf_Students on Groups.ID = Group_ConsistsOf_Students.Groups_ID where Group_ConsistsOf_Students.students_ID = " + userID + " )as b where b.RowNumber=1)) as count )"

	stmt, err := db.Query(chatsCount)

	if err != nil {
		fmt.Println("Query1 failed.....")
		fmt.Println(err.Error())
		return
	}
	if stmt.Next() {
		if err = stmt.Scan(&count); err != nil {
			fmt.Println("Scanning1 failed.....")
			fmt.Println(err.Error())
			return
		}
		fmt.Println("Count =", count)
		stmt.Close()

	}

	var querySelect = "((select  * from(select top 100  Users.id,CONCAT(Users.firstName , ' ' , Users.lastName) as Name, ROW_NUMBER() OVER (PARTITION BY Users.firstname ORDER BY Chats.date desc) AS RowNumber from Users join Chats on Users.id=Chats.user_receiver_id or Users.id=Chats.user_sender_id join(select top 100 max(Chats.date) maxtime, Chats.user_sender_id ,Chats.user_receiver_id,Chats.group_receiver_id from  Chats where Chats.user_sender_id =" + userID + "  or Chats.user_receiver_id =" + userID + " group by Chats.user_sender_id,Chats.user_receiver_id,Chats.group_receiver_id order by maxtime desc) latest on Chats.date=latest.maxtime  and Chats.user_sender_id=latest.user_sender_id where  users.id<>" + userID + " group by Users.ID ,Users.firstName,Users.lastName,Chats.date ) as a where a.RowNumber=1) union ALL(select *from (select top 100 Group_ConsistsOf_Students.Groups_ID,Groups.name, ROW_NUMBER() OVER (PARTITION BY Groups.name ORDER BY groups.id desc) AS RowNumber from Groups join Group_ConsistsOf_Students on Groups.ID = Group_ConsistsOf_Students.Groups_ID where Group_ConsistsOf_Students.students_ID = " + userID + " )as b where b.RowNumber=1)) "

	//fmt.Println(querySelect)

	rows, err := db.Query(querySelect)
	if err != nil {
		fmt.Println("Query failed.....")
		fmt.Println(err.Error())
		return
	}

	Users := make([]User, count)
	Chats := make([]Chat, count)

	i := 0
	j := 0
	for rows.Next() {
		var chat Chat
		var user User
		var k int
		if err = rows.Scan(&user.ID, &user.FirstName, &k); err != nil {
			fmt.Println("Scanning failed.....")
			fmt.Println(err.Error())
			return
		}

		Users[i] = user
		i++
		Chats[j] = chat
		j++

	}

	fmt.Printf("Users Instance := %#v\n", Users)
	json.NewEncoder(w).Encode(Users)

}

func setupCorsResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Authorization")
}

func handleRequests() {

	// Register routing for creating customer
	Config, _ := config.LoadConfiguration("conf.json")
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/user/chatList", returnChatList).Methods(http.MethodGet, http.MethodOptions).Name("Return users and groups Chat List ")
	myRouter.HandleFunc("/user/List", returnUsersGroupsList).Methods(http.MethodGet, http.MethodOptions).Name("Return users and all groups Chat List ")
	myRouter.HandleFunc("/chat/user/{correspondentID}", returnUserChat).Methods(http.MethodGet, http.MethodOptions).Name("Return user Chat ")
	myRouter.HandleFunc("/chat/group/{correspondentID}", returnGroupChat).Methods(http.MethodGet, http.MethodOptions).Name("Return Group Chat ")
	myRouter.HandleFunc("/user/chat/{key}", searchChatUser).Methods(http.MethodGet, http.MethodOptions).Name("Search User in Chat")
	myRouter.HandleFunc("/user/groupChat", returnUserGroups).Methods(http.MethodGet, http.MethodOptions).Name("Return User Groups")

	log.Fatal(http.ListenAndServe(":"+Config.Port+"", myRouter))
}
