package config

import (
	"encoding/json"
	"os"

	_ "github.com/denisenkom/go-mssqldb"
)

//Config ...
type Config struct {
	Database struct {
		Host     string `json:"host"`
		Port     string `json:"port"`
		Server   string `json:"server"`
		Database string `json:"database"`
		UserName string `json:"username"`
		Password string `json:"password"`
	} `json:"database"`

	Host string `json:"localhost"`
	Port string `json:"port"`
}

// LoadConfiguration - the function ...
func LoadConfiguration(filename string) (Config, error) {
	var config Config
	configFile, err := os.Open(filename)
	defer configFile.Close()
	if err != nil {
		return config, err
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	return config, err
}
