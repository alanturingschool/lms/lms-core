module gitlab.com/alanturingschool/lms/lms-core/Go/Chat

go 1.15

require (
	github.com/denisenkom/go-mssqldb v0.9.0
	github.com/gorilla/mux v1.8.0
)
