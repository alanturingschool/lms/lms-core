﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AlanTuring.Models
{
    public partial class GroupConsistsOfStudent
    {
        public int? StudentsId { get; set; }
        public int? GroupsId { get; set; }
    }
}
